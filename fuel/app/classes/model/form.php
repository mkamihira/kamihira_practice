<?php
class Model_Form extends Model
{
	// 投稿する
	public static function insert($row)
	{
		$mongo = \Mongo_Db::instance();

		// practiceコレクションにinsert
		$insert_id = $mongo->insert('practice',array(
			"user_name" => $row['user_name'],
			"write_area" => $row['write_area'],
			"timestamp" => time()
		));
	}

	// 投稿内容取得
	public static function get()
	{
		$mongo = \Mongo_Db::instance();

		// 投稿時間降順
		$mongo -> order_by(array(
			'timestamp' => -1
		));

		// get
		$result = $mongo -> get('practice');

		return $result;
	}

	// 編集
	public static function update($data)
	{
		$mongo = \Mongo_Db::instance();

		// 該当データを更新
		$bool = $mongo->where(array('_id' => new MongoId($data['mongo_id'])))
					  ->update('practice',array(
						"user_name" => $data['user_name'],
						"write_area" => $data['write_area'],
						// 編集日時追加
						"edit_time" => time()
						));
	}

	// 削除
	public static function delete($data)
	{
		$mongo = \Mongo_Db::instance();

		$bool = $mongo->where(array('_id' => new MongoId($data['mongo_id'])))
					  ->delete('practice');
	}
}