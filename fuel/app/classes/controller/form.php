<?php
class Controller_Form extends Controller
{
	// トップページ表示
	public function action_form()
  	{
  		// データ取得
  		$data = array();
  		$data['cursor'] = Model_Form::get();

  		// タイムゾーン設定
  		date_default_timezone_set('Asia/Tokyo');

  		// ビューに返す
    	return View::forge('form/index',$data);
  	}


    public function action_mng_form()
    {
      // データ取得
      $data = array();
      $data['cursor'] = Model_Form::get();

      // タイムゾーン設定
      date_default_timezone_set('Asia/Tokyo');

      // ビューに返す
      return View::forge('form/management',$data);
    }


 	// 投稿する
  	public function action_save()
  	{
  		$row = array();
  		$row = input::post();

  		Model_Form::insert($row);

  		Response::redirect('form/form');
  	}


    // 編集処理部分
    public function action_update()
    {
      $data = array();
      $data = input::post();

      Model_Form::update($data);

      Response::redirect('form/mng_form');
    }


    // 削除処理部分
    public function action_delete()
    {
      $data = array();
      $data = input::post();

      Model_Form::delete($data);

      Response::redirect('form/mng_form');
    }
}