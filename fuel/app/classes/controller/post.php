<?php
class Controller_Post extends Controller
{
  // insertする
  public function action_autoinsert()
  {
    for ($i=0; $i<10; $i++)
    {


      $row = array();
      $row['title'] = $i . '番目の投稿の件名';
      $row['summary'] = $i . '番目の投稿の概要';
      $row['body'] = 'これは' . $i . '番目の投稿です。' . "  \n" . 'テストで自動投稿しています。';


      Model_Post::insert($row);
    };

    echo "Finished!";
  }

  // 全件取得
  public function action_get()
  {
    $data =array();
    $data['rows'] = Model_Post::get();
        return View::forge('post/list',$data);
  }

  public function action_form()
  {
    return View::forge('post/form');
  }

  // 投稿する
  public function action_save()
  {
    $row = array();
    $row = Input::post();

    Model_post::insert($row);

    Response::redirect('post/form');
  }
}
