<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofllow">

        <!--jQueryの読み込み-->
        <!-- <script src="//code.jquery.com/jquery-1.11.0.min.js"></script> -->
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!--読み込まれたら次の処理をする-->
        <script>
            //$(document),ready(function(){
            $(function() {
                //".edit_area, .delete_area"を隠しておく
                $(".edit_area, .delete_area").hide();
                //クラスである".edit"のボタンをクリックしたとき
                $(".edit").click(function() {
                    //".delete_area"を隠しておく
                    $(".delete_area").hide();
                    // this=>".edit"　, slideToggle()でON/OFF切り替えたスライドをする
                    $(this).parents(".article").children(".edit_area").slideToggle();
                });
                //クラスである".delete"のボタンをクリックしたとき
                $(".delete").click(function() {
                    //".edit_area"を隠しておく
                    $(".edit_area").hide();
                    // this=>".delete"　, slideToggle()でON/OFF切り替えたスライドをする
                    $(this).parents(".article").children(".delete_area").slideToggle();
                });
            });
                //submit-eクラスのボタンが押されたときの処理
                //$('.submit_e').click(function() {
            function check() {
                //入力チェック
                if (!$('#ur').val().match(/\S/g) && !$('#wr').val().match(/\S/g)) {
                    $('#err_mes').text("どっちもからやんけ〜〜〜！！！").css('color','red').css('font-size',20);
                    return false;
                }
                else if (!$('#ur').val().match(/\S/g)) {
                    $('#err_mes').text("なまえ・・・・・").css('color','red').css('font-size',20);
                    return false;
                }
                else if (!$('#wr').val().match(/\S/g)) {
                    $('#err_mes').text("ほんぶん・・・・・・・・").css('color','red').css('font-size',20);
                    return false;
                }
            }
            </script>

        <link rel="icon" href="../assets/img/favicon.ico">
        <title>上平秘密の掲示板 | 管理画面</title>

        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
    </head>

    <body class="management">

        <?php
        // //error_reporting(0);
        // //接続
        // //Mongo
        // $mongo = new MongoClient();
        // //データベース
        // $db = $mongo->selectDB('practice_bbs_db');
        // //コレクション
        // $collection = $db->selectCollection('bbs');

        // 編集確定ボタン、削除確定ボタン押した時
        // if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        //     if (isset($_POST['submit_e'])) {
        //         $post_flg_e = '1';
        //         $mongo_id_e = htmlspecialchars($_POST['mongo_id'], ENT_QUOTES);
        //         $user_name = htmlspecialchars($_POST['user_name'], ENT_QUOTES);
        //         $write_area = htmlspecialchars($_POST['write_area'], ENT_QUOTES);
        //     }
        //     if (isset($_POST['submit_d'])) {
        //         $post_flg_d = '1';
        //         $mongo_id_d = htmlspecialchars($_POST['mongo_id'], ENT_QUOTES);
        //     }
        // }

        // **if ($post_flg_e === '1') {
        //     // postした値が空だったらアップデートしない(jQueryのalert処理との連携)
        //     if(strlen($user_name) === "" || strlen($write_area) === "")
        //     //アップデート('$set'=> array();を加えると置き換え。加えないと書き換え。)
        //     $collection->update(array('_id' => new MongoId($mongo_id_e)), array('$set' => array('user_name' => $user_name, 'write_area' => $write_area)));
        //     //$col->save($collection);
        //     //二重投稿対策
        //     header("Location: {$_SERVER['PHP_SELF']}");
        //     exit;
        // }

        // **if ($post_flg_d === '1') {
        //     //削除
        //     //var_dump($_POST['mongo_id']);exit();
        //     $collection->remove(array('_id' => new MongoId($mongo_id_d)));
        //     header("Location: {$_SERVER['PHP_SELF']}");
        //     exit;
        // }

        ?>

        <div class="header">
            <div class="contents">
                <h1 class="text_center title">掲示板管理ページ</h1>
                <div class="btn_area">
                    <a href="/form/form" target="_blank" class="btn btn-block btn-default">掲示板にもどる</a>
                </div>
            </div>
        </div>
        <div class="list">
            <div class="contents">

                <?php
                $count = 1;

                //配列の最後まで繰り返す
                foreach ($cursor as $value) {
                    //var_dump($value);
                    ?>

                    <div class="article">
                        <div class="article_over">
                            <span><?php echo $count; ?></span>
                            <span class="name">名前 : <?php echo $value['user_name'] ?></span>
                            <span class="date">　投稿日時：<?php echo date('Y年m月d日H時i分s秒', $value['timestamp']); ?></span>
                            <?php
                            // 編集日時があったら表示
                            if (array_key_exists('edit_time', $value)) {
                            ?>
                            <span class="edit_date">　編集日時：<?php echo date('Y年m月d日H時i分s秒', $value['edit_time']); ?></span>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="article_under">
                            <p><?php echo $value['write_area']; ?></p>
                        </div>
                        <div class="article_btns">
                            <div class="edit">
                                <button type="button" class="btn btn-block btn-default">編集するん？</button>
                            </div>
                            <div class="delete">
                                <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
                            </div>
                        </div>
                        <div class="edit_area">
                            <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
                            <form name="edit_form" role="form" action="/form/update" method="post">
                                <p name="err_mes" id="err_mes"></p>
                                <div class="form-group">
                                    <p name="name_txt" id="name_txt">名前：</p>
                                    <input name="user_name" type="text" id="ur" class="form-control user-name" maxlength="8" value=<?php echo $value['user_name']; ?>>
                                </div>
                                <div class="form-group">
                                    <p name="write_txt" id="write_txt">本文：</p>
                                    <textarea name="write_area" id="wr" class="form-control write-area" rows="5" cols="40" ><?php echo $value['write_area']; ?></textarea>
                                </div>
                                <!--nameでポストを飛ばす-->
                                <input type="hidden" name="mongo_id" value=<?php echo $value['_id']; ?>>
                                <input type="submit" name="submit_e" class="btn btn-block btn-primary submit-e" value="編集しちゃう！">
                            </form>
                        </div>
                        <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
                        <form class="delete_area" action="/form/delete" method="post">
                            <p class="text_center">消しちゃいますよー？いいっすか？</p>
                            <!--nameでポストを飛ばす-->
                            <input type="hidden" name="mongo_id" value=<?php echo $value['_id']; ?>>
                            <input type="submit" name="submit_d" class="btn btn-block btn-danger submit-d" value="消しちゃいましょう！">
                        </form>
                        <hr>
                    </div>
                    <?php
                    $count++;
                }
                ?>
                <!--
                <div class="article">
                        <div class="article_over">
                                <span>001</span>
                                <span class="name">名前 : 佐藤孝則ああああ</span>
                                <span class="date">2014/05/09(金)</span>
                                <span class="time">投稿時間 : 10:50:34</span>
                        </div>
                        <div class="article_under">
                                <p>あーー。まじ北上は酒の売れ行きパねえ。</p>
                        </div>
                        <div class="article_btns">
                                <div class="edit">
                                        <button type="button" class="btn btn-block btn-default">編集する</button>
                                </div>
                                <div class="delete">
                                        <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
                                </div>
                        </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>001</span>
                        <span class="name">名前 : 佐藤孝則ああああ</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 10:50:34</span>
                    </div>
                    <div class="article_under">
                        <p>あーー。まじ北上は酒の売れ行きパねえ。</p>
                    </div>
                    <div class="article_btns">
                        <div class="edit">
                                <button type="button" class="btn btn-block btn-default">編集する</button>
                        </div>
                        <div class="delete">
                                <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
                        </div>
                    </div>
                    <div class="edit_area">
                        <form role="form">
                                <div class="form-group">
                                        <input type="text" class="form-control" maxlength="8" placeholder="Enter your beautiful name">
                                </div>
                                <div class="form-group">
                                        <textarea class="form-control" rows="5" cols="40"></textarea>
                                </div>
                        </form>
                        <button type="button" class="btn btn-block btn-primary">編集する</button>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>001</span>
                        <span class="name">名前 : 佐藤孝則ああああ</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 10:50:34</span>
                    </div>
                    <div class="article_under">
                        <p>あーー。まじ北上は酒の売れ行きパねえ。</p>
                    </div>
                    <div class="article_btns">
                        <div class="edit">
                            <button type="button" class="btn btn-block btn-default">編集する</button>
                        </div>
                        <div class="delete">
                            <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
                        </div>
                    </div>
                    <div class="delete_area">
                        <p class="text_center">消しちゃいますよー？いいっすか？</p>
                        <button type="button" class="btn btn-block btn-danger">消しちゃって！</button>
                    </div>
                </div>
                -->
            </div>
        </div>
    </body>

 
    <script>
        // 二重投稿対策処理  
            $("form").submit(function() {
                $(".submit-e").prop("disabled", true);
                $(".submit-d").prop("disabled",true);
                var bool = check();
                if (bool === false) {
                    $(".submit-e").prop("disabled", false);
                    return false;
                }
                else {
                    return true;
                }
            });
    </script>
</html>