<?php

//抽象クラス

// abstract class BaseUser{
// 	public $name;
// 	abstract public function sayHi();
// }

// class User extends BaseUser{
// 	public function sayHi(){
// 		echo "hello from User";
// 	}
// }

// User::sayHi();

//


//interface

interface sayHi{
	public function sayHi();
}

interface sayHello{
	public function sayHello();
}


class User implements sayHi,sayHello{
	public function sayHi(){
		echo "hi!";
	}

	public function sayHello(){
		echo "hello!";
	}
}