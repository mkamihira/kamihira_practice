<?php

class User{
	public $name;
	public static $count = 0; //インスタンス化されるたびに一つ増やす

	public function __construct($name){ //コンストラクタ
		$this->name = $name;
		self::$count++;
	}

		public function sayHi(){
		echo "hi i am $this->name!";
	}

	public static function getMessage(){
		echo "hello from User class!";
	}
}

class AdminUser extends User // 継承
{
	public function sayHello(){
		echo "hello from again!";
	}

	public function sayHi(){
		echo "[admin] hi i am $this->name!"; //override
	}
}

// User::getMessage();

$tom = new User("Tom");
$bob = new User("Bob");

echo User::$count;

// $tom = new User("Tom");
// $steave= new Adminuser("Steave");
// echo $steave->name;
// $steave->sayHi();
// $tom->sayHi();
// $steave->sayHello();