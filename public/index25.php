<?php

//require / require_once   error

//include　warning

//autoload class　クラスのみ


// require "User_class.php";

// 	require $class . "_class.php";
// });

// $bob = new User("Bob");
// $bob->sayHi();



// 例外処理
function div($a,$b){
	try{
		if($b == 0){
			throw new Exception("cannot divide by 0");
		}
		echo $a/$b;
	}
	catch(exception $e){
		echo $e->getMessage();
	}
}

div(7,2);
div(5,0);